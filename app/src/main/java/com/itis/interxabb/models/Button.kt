package com.itis.interxabb.models

import java.io.Serializable

data class Button(val sort:Int,
                  val title:String,
                  val action:Int) : Serializable