package com.itis.interxabb.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.itis.interxabb.R
import com.itis.interxabb.application.App
import com.itis.interxabb.ui.router.PageRouter
import com.itis.interxabb.utils.managers.ProfileManager
import kotlinx.android.synthetic.main.app_bar_main.*

class ProfileSettingsFragment : Fragment() {

    private lateinit var profileManager: ProfileManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity!!.toolbar.title = getString(R.string.profile)
        profileManager = (activity!!.application as App).getProfileManager()

        return inflater.inflate(R.layout.profile_settings_fragment, container, false)
    }

    override fun onViewCreated(viewFragment: View, savedInstanceState: Bundle?) {
        super.onViewCreated(viewFragment, savedInstanceState)

        val inputUsernameView:EditText = viewFragment.findViewById(R.id.input_username_view) ?: return
        inputUsernameView.text = Editable
            .Factory
            .getInstance()
            .newEditable(profileManager.getUsername())

        val inputPasswordView:EditText = viewFragment.findViewById(R.id.input_password_view) ?: return
        inputPasswordView.text = Editable
            .Factory
            .getInstance()
            .newEditable(profileManager.getPassord())

        val saveButton:Button = viewFragment.findViewById(R.id.save_button)
        saveButton.setOnClickListener {
            val enteredUsername:String = inputUsernameView.text.toString()
            val enteredPassword:String = inputPasswordView.text.toString()

            if(!enteredUsername.isEmpty() && !enteredPassword.isEmpty()) {
                profileManager.saveProfile(enteredUsername, enteredPassword)
            }
            else {
                if(enteredUsername.isEmpty()) {
                    inputUsernameView.error = getString(R.string.this_field_should_not_be_empty)
                }

                if (enteredPassword.isEmpty()) {
                    inputPasswordView.error = getString(R.string.this_field_should_not_be_empty)
                }
            }

            val router = PageRouter(this)
            router.openFragmentGone(R.id.fragment_container, ChatFragment(), null)
        }
    }
}