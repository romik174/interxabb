package com.itis.interxabb.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.itis.interxabb.R
import com.itis.interxabb.application.App
import com.itis.interxabb.ui.router.PageRouter
import com.itis.interxabb.utils.managers.ConnectionManager
import com.itis.interxabb.utils.managers.ConnectionParams
import kotlinx.android.synthetic.main.app_bar_main.*

class ConnectionSettingsFragment : Fragment() {

    private lateinit var connectionManager: ConnectionManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity!!.toolbar.title = getString(R.string.connection)
        connectionManager = (activity!!.application as App).getConnectionManager()
        return inflater.inflate(R.layout.connection_settings_fragment, container, false)
    }

    override fun onViewCreated(viewFragment: View, savedInstanceState: Bundle?) {
        super.onViewCreated(viewFragment, savedInstanceState)

        val connectionParams:ConnectionParams = connectionManager.getConnectionParams()

        val inputHostView:EditText = viewFragment.findViewById(R.id.input_host_view) ?: return
        inputHostView.text = toEditable(connectionParams.host)

        val inputPortView:EditText = viewFragment.findViewById(R.id.input_port_view) ?: return
        inputPortView.text = toEditable(connectionParams.port.toString())

        val inputDomainView:EditText = viewFragment.findViewById(R.id.input_domain_view) ?: return
        inputDomainView.text = toEditable(connectionParams.domain)

        val inputResourceView:EditText = viewFragment.findViewById(R.id.input_resource_view) ?: return
        inputResourceView.text = toEditable(connectionParams.resource)

        val saveButton:Button = viewFragment.findViewById(R.id.save_button)
        saveButton.setOnClickListener {
            val enteredHost:String     = inputHostView.text.toString()
            val enteredPort:String     = inputPortView.text.toString()
            val enteredDomain:String   = inputDomainView.text.toString()
            val enteredResource:String = inputResourceView.text.toString()

            if(!enteredHost.isEmpty() && !enteredPort.isEmpty() && !enteredDomain.isEmpty() && !enteredResource.isEmpty()) {
                connectionManager.saveConnectionParams(
                    enteredHost,
                    enteredPort.toInt(),
                    enteredDomain,
                    enteredResource
                )
            }
            else {
                if(enteredHost.isEmpty()) {
                    inputHostView.error = getString(R.string.this_field_should_not_be_empty)
                }

                if(enteredPort.isEmpty()) {
                    inputPortView.error = getString(R.string.this_field_should_not_be_empty)
                }

                if(enteredDomain.isEmpty()) {
                    inputDomainView.error = getString(R.string.this_field_should_not_be_empty)
                }

                if(enteredResource.isEmpty()) {
                    inputResourceView.error = getString(R.string.this_field_should_not_be_empty)
                }
            }

            val router = PageRouter(this)
            router.openFragmentGone(R.id.fragment_container, ChatFragment(), null)
        }
    }

    private fun toEditable(string: String) : Editable {
        return Editable.Factory.getInstance().newEditable(string)
    }
}