package com.itis.interxabb.ui.adapters

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.itis.interxabb.R
import com.itis.interxabb.models.Button


class ButtonViewHolder(view: View) : RecyclerView.ViewHolder(view)

interface ButtonOnClickListener {
    fun onClick(button:Button)
}

class ButtonsAdapter(private val context: Context,
                     private val buttons: ArrayList<Button>,
                     private val buttonOnClickListener: ButtonOnClickListener) : RecyclerView.Adapter<ButtonViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): ButtonViewHolder {
        return ButtonViewHolder(
            LayoutInflater.from(context).inflate(R.layout.buttons_list_item, container, false)
        )
    }

    override fun onBindViewHolder(buttonViewHolder: ButtonViewHolder, position: Int) {
        if(position > buttons.size || position < 0) return

        val btn = buttons[position]
        val buttonView:TextView = buttonViewHolder.itemView.findViewById(R.id.button_view)

        if(position == buttons.size - 1) {
            val height:Int = context.resources.getDimension(R.dimen.button_dialog_height).toInt()

            val params:ConstraintLayout.LayoutParams = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                height
            )

            val scale:Float = context.resources.displayMetrics.density
            params.bottomMargin = (scale * context.resources.getDimension(R.dimen.small_vertical_margin)).toInt()
            buttonView.layoutParams = params
        }

        buttonView.text = btn.title
        buttonView.setOnClickListener {
            buttonOnClickListener.onClick(btn)
        }
    }

    override fun getItemCount(): Int {
        return buttons.size
    }

    fun clear() {
        buttons.clear()
        notifyDataSetChanged()
    }
}