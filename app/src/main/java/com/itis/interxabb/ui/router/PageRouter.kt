package com.itis.interxabb.ui.router

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

class PageRouter{

    private var fragmentManager: FragmentManager

    constructor(activity: AppCompatActivity) {
        fragmentManager = activity.supportFragmentManager
    }

    constructor(fragment: Fragment) {
        fragmentManager = fragment.fragmentManager!!
    }

    fun startFragment(container: Int, fragment: Fragment) {
        fragmentManager.beginTransaction()
            .add(container, fragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    fun openFragmentGone(container: Int, fragment: Fragment, args: Bundle?) {
        fragment.arguments = args

        fragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(container, fragment)
            .commit()
    }

    fun openFragment(container: Int, fragment: Fragment, args: Bundle?) {
        fragment.arguments = args

        fragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(container, fragment)
            .addToBackStack(null)
            .commit()
    }
}