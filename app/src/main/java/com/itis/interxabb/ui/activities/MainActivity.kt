package com.itis.interxabb.ui.activities

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.itis.interxabb.R
import com.itis.interxabb.application.App
import com.itis.interxabb.ui.fragments.ChatFragment
import com.itis.interxabb.ui.fragments.ConnectionSettingsFragment
import com.itis.interxabb.ui.fragments.ProfileSettingsFragment
import com.itis.interxabb.ui.router.PageRouter
import com.itis.interxabb.utils.managers.ProfileManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var router:PageRouter
    private lateinit var profileManager:ProfileManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        profileManager = (application as App).getProfileManager()

        val toggle = object: ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                val usernameView:TextView = drawerView.findViewById(R.id.nav_username)
                usernameView.text = profileManager.getUsername()
            }
        }
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        router = PageRouter(this)
        router.startFragment(
            R.id.fragment_container,
            ChatFragment()
        )
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.dialogs -> {
                router.openFragment(
                    R.id.fragment_container,
                    ChatFragment(),
                    null
                )
            }
            R.id.profile_settings -> {
                router.openFragment(
                    R.id.fragment_container,
                    ProfileSettingsFragment(),
                    null
                )
            }
            R.id.connection_settings -> {
                router.openFragment(
                    R.id.fragment_container,
                    ConnectionSettingsFragment(),
                    null
                )
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
