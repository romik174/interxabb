package com.itis.interxabb.ui.fragments

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.itis.interxabb.R
import com.itis.interxabb.application.App
import com.itis.interxabb.models.Button
import com.itis.interxabb.ui.adapters.ButtonOnClickListener
import com.itis.interxabb.ui.adapters.ButtonsAdapter
import com.itis.interxabb.ui.adapters.Message
import com.itis.interxabb.ui.adapters.MessageAdapter
import com.itis.interxabb.ui.presenters.ChatFragmentPresenter

class ChatFragment : Fragment() {

    private lateinit var presenter: ChatFragmentPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val app:App = activity!!.application as App

        presenter = ChatFragmentPresenter(
            this,
            app.getProfileManager(),
            app.getConnectionManager()
        )

        return inflater.inflate(R.layout.chat_fragment, container, false)
    }

    private lateinit var dialogView:RecyclerView
    private lateinit var inputView:EditText
    private lateinit var buttonSend:ImageButton
    private lateinit var progressBar:ProgressBar
    private lateinit var buttonsView:RecyclerView

    private lateinit var adapter: MessageAdapter

    override fun onViewCreated(viewFragment: View, savedInstanceState: Bundle?) {
        super.onViewCreated(viewFragment, savedInstanceState)

        dialogView = viewFragment.findViewById(R.id.dialog_view)

        dialogView.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )

        val messages = ArrayList<Message>()
        adapter = MessageAdapter(
            context  = context?:return,
            messages = messages
        )
        dialogView.adapter = adapter

        inputView = viewFragment.findViewById(R.id.input_view)
        inputView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) { }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(!s!!.isEmpty() && s.length == 3) {
                    presenter.notifyStateTyping()
                }
            }
        })

        buttonSend  = viewFragment.findViewById(R.id.button_send_view)
        buttonSend.setOnClickListener { presenter.sendMessage() }

        progressBar = viewFragment.findViewById(R.id.progress_bar)

        buttonsView = viewFragment.findViewById(R.id.buttons_view)
        buttonsView.layoutManager = LinearLayoutManager(context!!, LinearLayout.VERTICAL, false)

        presenter.connectToServer()
    }

    override fun onPause() {
        super.onPause()
        presenter.disconnectFromServer()
    }

    fun showMessage(message: String) {
        activity!!.runOnUiThread {
            Snackbar.make(view?.rootView?:return@runOnUiThread, message, Snackbar.LENGTH_LONG)
                .show()
        }
    }

    fun showSentMessage(author:String, messageId:String, messageText: String, time:String, status: Boolean?) {
        activity!!.runOnUiThread {
            adapter.addSentMessage(
                author         = author,
                messageId      = messageId,
                messageText    = messageText,
                time           = time,
                deliveryStatus = status,
                readStatus     = null
            )
            updateUI()
            inputView.text.clear()
        }
    }

    fun showIncomingMessage(author:String, messageId: String, messageText: String, time:String) {
        activity!!.runOnUiThread {
            adapter.addReceivedMessage(
                author      = author,
                messageId   = messageId,
                messageText = messageText,
                time        = time
            )
            updateUI()
        }
    }

    fun getInputText(): String {
        return inputView.text.toString()
    }

    private fun updateUI() {
        activity!!.runOnUiThread {
            dialogView.viewTreeObserver.addOnGlobalLayoutListener {
                dialogView.smoothScrollToPosition(adapter.itemCount - 1)
                dialogView.viewTreeObserver.removeOnGlobalLayoutListener { this }
            }
        }
    }

    fun showProgress() {
        activity!!.runOnUiThread {
            if(progressBar.visibility != View.VISIBLE)
                progressBar.visibility = View.VISIBLE
        }
    }

    fun hideProgress() {
        activity!!.runOnUiThread {
            if(progressBar.visibility != View.INVISIBLE)
                progressBar.visibility = View.INVISIBLE
        }
    }

    fun setDeliveryMessageStatus(messageId: String, status:Boolean) {
        activity!!.runOnUiThread {
            adapter.setStatusDeliveryMessage(
                messageId = messageId,
                status    = status
            )
        }
    }

    fun setReadMessageStatus(messageId: String, status: Boolean) {
        activity!!.runOnUiThread {
            adapter.setStatusReadMessage(
                messageId = messageId,
                status    = status
            )
        }
    }

    fun showTyping(author: String, messageId: String) {
        activity!!.runOnUiThread {
            adapter.addTypingMessage(
                author    = author,
                messageId = messageId
            )
        }
    }

    fun showDialogButtons(buttons: ArrayList<Button>?, buttonOnClickListener: ButtonOnClickListener) {
        activity!!.runOnUiThread {
            if(buttons == null || buttons.size == 0) {
                hideDialogButtons()
                return@runOnUiThread
            }

            val scale:Float = resources.displayMetrics.density
            val height:Int = (scale * if(buttons.size <= 3) {
                buttons.size * 43 + 7
            } else 136).toInt()

            val adapter = ButtonsAdapter(context!!, buttons, buttonOnClickListener)
            buttonsView.adapter = adapter
            buttonsView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height)
        }
    }

    fun hideDialogButtons() {
        buttonsView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0)
        (buttonsView.adapter as ButtonsAdapter).clear()
    }

    fun removeTyping() {
        activity!!.runOnUiThread { adapter.removeTypingMessage() }
    }
}