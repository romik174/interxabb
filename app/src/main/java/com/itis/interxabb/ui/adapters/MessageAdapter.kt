package com.itis.interxabb.ui.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.itis.interxabb.R
import com.itis.interxabb.ui.adapters.Message.Companion.INCOMING_MESSAGE
import com.itis.interxabb.ui.adapters.Message.Companion.SENT_MESSAGE
import com.itis.interxabb.ui.adapters.Message.Companion.TYPING_MESSAGE

data class Message(val type:String,
                   val author:String,
                   val messageId:String,
                   val message:String,
                   val time:String,
                   var deliveryStatus: Boolean?,
                   var readStatus:Boolean?) {
    companion object {
        const val INCOMING_MESSAGE = "received"
        const val SENT_MESSAGE     = "sent"
        const val TYPING_MESSAGE   = "typing"
    }

    override fun equals(other: Any?): Boolean {
        return other is Message && messageId == other.messageId
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

class MessageAdapter(private val context: Context?,
                     private val messages: ArrayList<Message>) : RecyclerView.Adapter<ViewHolder>() {

    private val statusMessages = HashMap<String, ImageView>()

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): ViewHolder {
        if(viewType == -1)
            return ViewHolder(View(context))
        return ViewHolder(
            LayoutInflater.from(context).inflate(viewType, container, false)
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if(getItemViewType(position) == - 1) return


        val message:Message = messages[position]


        if(getItemViewType(position) == R.layout.user_typing_state) {
            val username: AppCompatTextView = viewHolder.itemView.findViewById(R.id.username) ?: return
            username.text = message.message
            return
        }


        if(getItemViewType(position) == R.layout.firts_incoming_message) {
            val authorView: AppCompatTextView = viewHolder.itemView.findViewById(R.id.author_view)
            authorView.text = message.author
        }


        if(position > 0 && position != messages.size &&
            message.type == INCOMING_MESSAGE &&
            message.type != messages[position - 1].type) {

            val authorView = viewHolder
                .itemView
                .findViewById<TextView>(R.id.author_view) ?: return
            authorView.text = message.author
        }


        val messageView = viewHolder
            .itemView
            .findViewById<AppCompatTextView>(R.id.message_view) ?: return
        messageView.text = message.message


        val timeView = viewHolder
            .itemView
            .findViewById<TextView>(R.id.time_view) ?: return
        timeView.text = message.time


        if(message.deliveryStatus != null) {
            val messageStatusView:ImageView = viewHolder
                .itemView.findViewById(
                R.id.message_delivery_status_view
            ) ?: return

            if(message.deliveryStatus ?: return) {
                messageStatusView.visibility = VISIBLE
                messageStatusView.setImageDrawable(
                    ContextCompat.getDrawable(
                        this.context!!,
                        R.drawable.ic_delivery
                    )
                )
            }
        }


        if(message.readStatus != null) {
            val messageStatusView:ImageView = viewHolder
                .itemView.findViewById(
                R.id.message_delivery_status_view
            ) ?: return

            if(message.deliveryStatus ?: return) {
                if(messageStatusView.visibility == INVISIBLE) return

                messageStatusView.setImageDrawable(
                    ContextCompat.getDrawable(
                        this.context!!,
                        R.drawable.ic_all
                    )
                )
            }
        }


        if(message.type == SENT_MESSAGE) {
            val messageStatusView: ImageView  = viewHolder.itemView.findViewById(R.id.message_delivery_status_view)
            statusMessages[message.messageId] = messageStatusView
        }
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun getItemViewType(position: Int): Int {
        val message:Message = messages[position]
        when(message.type) {
            SENT_MESSAGE -> return if(position > 0 && messages[position - 1].type == message.type)
                    R.layout.second_sent_message
                else R.layout.first_sent_message

            INCOMING_MESSAGE -> return  if(position > 0 && messages[position - 1].type == message.type &&
                    message.author == messages[position - 1].author)
                    R.layout.second_incoming_message
                else R.layout.firts_incoming_message

            TYPING_MESSAGE -> return R.layout.user_typing_state
        }

        return -1
    }

    fun addSentMessage(author: String,
                       messageId:String,
                       messageText: String,
                       time: String,
                       deliveryStatus: Boolean?,
                       readStatus: Boolean?) {

        val message = Message(
            type           =  SENT_MESSAGE,
            author         =  author,
            messageId      = messageId,
            message        =  messageText,
            time           = time,
            deliveryStatus = deliveryStatus,
            readStatus     = readStatus
        )

        if(messages.contains(message)) return

        messages.add(message)
        notifyDataSetChanged()
    }

    fun addReceivedMessage(author: String, messageId: String, messageText: String, time: String) {
        val message = Message(
            type           =  INCOMING_MESSAGE,
            author         =  author,
            messageId      =  messageId,
            message        =  messageText,
            time           =  time,
            deliveryStatus = null,
            readStatus     = null
        )

        if(messages.contains(message)) return

        messages.add(message)
        notifyDataSetChanged()
    }

    fun setStatusDeliveryMessage(messageId: String, status:Boolean) {
        val msg:Message? =  messages
            .find { message -> message.messageId == messageId  }

        if(msg != null) {
            msg.deliveryStatus = status
            notifyDataSetChanged()
        }
    }

    fun setStatusReadMessage(messageId: String, status: Boolean) {
        val msg:Message? = messages
            .find { message -> messageId == message.messageId }

        if(msg != null) {
            msg.readStatus = status
            notifyDataSetChanged()
        }
    }

    fun addTypingMessage(author: String, messageId: String) {
        for (message:Message in messages) {
            if(message.type == TYPING_MESSAGE) return
        }

        val message = Message(
            type           =  TYPING_MESSAGE,
            author         =  author,
            messageId      =  messageId,
            message        =  String.format("%s %s...", author, context!!.getString(R.string.typing_message)),
            time           = "",
            deliveryStatus = null,
            readStatus     = null
        )

        if(messages.contains(message)) return

        messages.add(message)
        notifyDataSetChanged()
    }

    fun removeTypingMessage() {
        if(messages.size == 0) return

        for (message:Message in messages) {
            if (message.type == TYPING_MESSAGE) {
                messages.remove(message)
                notifyDataSetChanged()
                return
            }
        }
    }
}

