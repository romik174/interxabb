package com.itis.interxabb.ui.presenters

import android.annotation.SuppressLint
import android.os.Handler
import android.util.Log
import com.itis.interxabb.R
import com.itis.interxabb.models.Button
import com.itis.interxabb.ui.adapters.ButtonOnClickListener
import com.itis.interxabb.ui.fragments.ChatFragment
import com.itis.interxabb.utils.api.ApiAuthService
import com.itis.interxabb.utils.api.ApiConnection
import com.itis.interxabb.utils.api.UserInfo
import com.itis.interxabb.utils.managers.ConnectionManager
import com.itis.interxabb.utils.managers.ConnectionParams
import com.itis.interxabb.utils.managers.ProfileManager
import com.itis.interxabb.utils.xmpp.EjabberdClient
import com.itis.interxabb.utils.xmpp.chat.EjabberdChatsClient
import com.itis.interxabb.utils.xmpp.chat.MUChatStateManager
import com.itis.interxabb.utils.xmpp.chat.StateChatTimeConfig
import com.itis.interxabb.utils.xmpp.messages.*
import org.jivesoftware.smack.ConnectionListener
import org.jivesoftware.smack.MessageListener
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smackx.chatstates.ChatState

class ChatFragmentPresenter(private val fragment:ChatFragment,
                            profileManager: ProfileManager,
                            connectionManager: ConnectionManager) {

    private val connectionParams:ConnectionParams = connectionManager.getConnectionParams()

    private val ejabberdHost = connectionParams.host
    private val port         = connectionParams.port
    private val domain       = connectionParams.domain
    private val resource     = connectionParams.resource

    private val username     = profileManager.getUsername()
    private val password     = profileManager.getPassord()


    private var chatName: String? = null

    private val apiHost = "http://192.168.14.180:3000"

    private var ejabberdClient = EjabberdClient(
        fragment.activity,
        ejabberdHost,
        port,
        domain,
        resource,
        EjabberdClient.TLS_TYPE_CONNECTION
    )

    private var chatClient: EjabberdChatsClient? = null
    private var mucChatStateManager: MUChatStateManager? = null

    private val apiConnection = ApiConnection.create(apiHost)
    private val authService   = ApiAuthService(apiConnection)

    fun connectToServer() {
        fragment.showProgress()

        authService.auth(username, object : ApiAuthService.AuthCallback {
            override fun onAuth(userInfo: UserInfo) {
                Log.e(javaClass.name, String.format("auth in api server: %s", userInfo.userJID))

                ejabberdClient.connect(username, password, object: ConnectionListener {

                    override fun connected(connection: XMPPConnection?) {
                        Log.i(javaClass.name, "connect to ejabberd server")
                    }

                    override fun authenticated(connection: XMPPConnection?, resumed: Boolean) {
                        Log.i(javaClass.name, "auth in ejabberd server")

                        chatName = "31aab982e24b091cf3c84fbd980ab83a@conference.localhost"

                        enableStateChatListener(connection = connection?:return)
                        enableMessageListener( chatJid = chatName?: return)
                    }

                    override fun connectionClosed() {
                        Log.i(javaClass.name, "ejabberd connection closed")
                        notifyStatePaused()
                        fragment.showMessage(fragment.getString(R.string.connection_closed))
                        ejabberdClient.reconnect()
                    }

                    override fun connectionClosedOnError(e: Exception?) {
                        Log.e(javaClass.name, e!!.message)
                        e.printStackTrace()
                        notifyStatePaused()
                        fragment.showMessage(fragment.getString(R.string.connection_error))
                        ejabberdClient.reconnect()
                    }
                })
            }

            override fun onError() {
                Log.e(javaClass.name, "error auth in api server")
                fragment.showMessage(fragment.getString(R.string.auth_error))
            }
        })
    }

    fun sendMessage() {
        if(!ejabberdClient.isConnection) return

        val messageText:String = fragment.getInputText()
        if(messageText.isEmpty()) return

        try {
            val messageId:String = chatClient!!.sendMessage(chatName ?: return, messageText) { _, _, receiptId, _ ->
                fragment.setDeliveryMessageStatus(receiptId, true)
            }

            fragment.showSentMessage(
                author      = username,
                messageId   = messageId,
                messageText = messageText,
                time        = getCurrentTimeString(fragment.context!!),
                status      = null
            )
        }
        catch (e: java.lang.Exception) {
            e.printStackTrace()
            Log.e(javaClass.name, e.message)
        }
    }

    fun disconnectFromServer() {
        authService.cancel()
        notifyStateGone()

        if(ejabberdClient.isConnection) {
            ejabberdClient.disconnect()
            chatClient?.disconnectChats()
            mucChatStateManager?.removeChatStateListener(mucChatStateListener)
        }
    }

    fun notifyStateTyping() {
        if(chatClient != null && chatClient!!.isConnection)
            chatClient!!.notifyStatusChat(
                chatName?: return,
                ChatState.composing
            )
    }

    fun notifyStatePaused() {
       if(chatClient?.isConnection ?: return) {
           chatClient?.notifyStatusChat(chatName?: return, ChatState.paused)
       }
    }

    private fun notifyStateGone() {
        if(chatClient?.isConnection ?: return) {
            chatClient?.notifyStatusChat(chatName?: return, ChatState.gone)
        }
    }

    private val handlerChatState = Handler(Handler.Callback {
        fragment.removeTyping()
        return@Callback false
    })

    private fun enableMessageListener(chatJid:String) {
        chatClient = EjabberdChatsClient(ejabberdClient)
        chatClient!!.openGroupChat(chatJid, username, messageListener)
    }


    private fun enableStateChatListener(connection:XMPPConnection) {
        mucChatStateManager = MUChatStateManager.getInstance(connection)
        mucChatStateManager!!.addChatStateListener(mucChatStateListener)
    }

    @SuppressLint("SimpleDateFormat")
    private val messageListener = MessageListener { message: Message? ->
        val messageText:String = message!!.body ?: return@MessageListener

        val sender:String = message.from.resourceOrEmpty!!.toString()

        if(messageText.isEmpty()) {
            if(isNotifyReadMessageStatus(username, message)) {
                fragment.setReadMessageStatus(
                    messageId = message.stanzaId,
                    status = true
                )
            }
            return@MessageListener
        }

        if(isNotifyDeliveredMessage(username, message)) {
            chatClient?.notifyStatusDeliveredMessage(chatName, message.stanzaId) ?: return@MessageListener
        }

        if(isButtonCommandMessage(username, message)) {
            val buttons:ArrayList<Button> = getButtonCommands(message) ?: return@MessageListener
            fragment.showDialogButtons(buttons, dialogButtonClickListener)
        }

        this@ChatFragmentPresenter.fragment.activity!!.runOnUiThread{

            fragment.hideProgress()

            if(sender != username) {
                fragment.showIncomingMessage(
                    author      = sender,
                    messageId   = message.stanzaId,
                    messageText = messageText,
                    time        = getDelayTime(context = fragment.context!!, message = message)
                )
            } else {
                fragment.showSentMessage(
                    author      = username,
                    messageId   = message.stanzaId,
                    messageText = messageText,
                    time        = getDelayTime(context = fragment.context!!, message = message),
                    status      = true
                )

                fragment.setDeliveryMessageStatus(
                    messageId =  message.stanzaId,
                    status    = true
                )
            }
        }
    }

    private val mucChatStateListener = MUChatStateManager.MUChatStateListener { _, finalState, message ->

        if(message.body != null && message.body.isEmpty()) {
            this@ChatFragmentPresenter.fragment.activity!!.runOnUiThread {

                fragment.hideProgress()

                val username:String = message.from.resourceOrEmpty!!.toString()

                if(this.username != username) {
                    when (finalState) {
                        ChatState.composing -> {
                            fragment.showTyping(username, message.stanzaId)
                            handlerChatState.sendEmptyMessageDelayed(
                                StateChatTimeConfig.handlerMessage,
                                StateChatTimeConfig.delay
                            )
                        }
                        ChatState.paused -> {

                        }
                        ChatState.gone -> {
                            fragment.removeTyping()
                            if(handlerChatState.hasMessages(StateChatTimeConfig.handlerMessage)) {
                                handlerChatState.removeMessages(StateChatTimeConfig.handlerMessage)
                            }
                        }
                        else -> {}
                    }
                }
            }
        }
    }

    private val dialogButtonClickListener = object : ButtonOnClickListener {
        override fun onClick(button: Button) {
            fragment.hideDialogButtons()
            chatClient!!.sendCommand(chatName, button)
        }
    }
}