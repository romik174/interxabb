package com.itis.interxabb.utils.managers

import android.content.Context
import android.content.SharedPreferences

data class ConnectionParams(val host:String,
                            val port:Int,
                            val domain:String,
                            val resource:String)

class ConnectionManager(context: Context) {
    companion object {
        private const val preferencesName = "connection_preferences"
        private const val hostKey         = "host"
        private const val portKey         = "port"
        private const val domainKey       = "domain"
        private const val resourceKey     = "resource"

        private const val defaultHost     = "192.168.14.180"
        private const val defaultPort     = "5222"
        private const val defaultDomain   = "localhost"
        private const val defaultResource = "xmlpp"
    }

    private val connectionPreferences:SharedPreferences = context
        .getSharedPreferences(preferencesName, Context.MODE_PRIVATE)

    fun saveConnectionParams(host: String, port: Int, domain: String, resource: String) {
        connectionPreferences.edit()
            .putString(hostKey, host)
            .putInt(portKey, port)
            .putString(domainKey, domain)
            .putString(resourceKey, resource)
            .apply()
    }

    fun getConnectionParams() : ConnectionParams {
        val host:String     = connectionPreferences.getString(hostKey, defaultHost) ?: ""
        val port:Int        = (connectionPreferences.getString(portKey, defaultPort) ?: "").toInt()
        val domain:String   = connectionPreferences.getString(domainKey, defaultDomain) ?: ""
        val resource:String = connectionPreferences.getString(resourceKey, defaultResource) ?: ""

        return ConnectionParams(host, port, domain, resource)
    }
}