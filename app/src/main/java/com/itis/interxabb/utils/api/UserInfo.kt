package com.itis.interxabb.utils.api

data class UserInfo(val userJID:String,
                    val chatJID:String)