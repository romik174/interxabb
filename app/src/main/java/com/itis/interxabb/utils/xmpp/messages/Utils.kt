package com.itis.interxabb.utils.xmpp.messages

import android.annotation.SuppressLint
import android.content.Context
import com.beust.klaxon.Klaxon
import com.itis.interxabb.R
import com.itis.interxabb.models.Button
import org.jivesoftware.smack.packet.ExtensionElement
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.packet.StandardExtensionElement
import org.jivesoftware.smackx.delay.packet.DelayInformation
import org.jivesoftware.smackx.receipts.DeliveryReceipt
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SimpleDateFormat")
fun getCurrentTimeString(context: Context):String {
    val currTime: Long = Calendar.getInstance().time.time
    val timeFormat = SimpleDateFormat(context.getString(R.string.message_time_format))

    return timeFormat.format(currTime)
}

@SuppressLint("SimpleDateFormat")
fun getDelayTime(context: Context, message:Message) : String {
    val delayInfo: DelayInformation? = message.getExtension(DelayInformation.NAMESPACE) as DelayInformation?
    val time = delayInfo?.stamp?.time ?: 0

    return if(time == 0L) {
        getCurrentTimeString(context)
    } else {
        val simpleDateFormat = SimpleDateFormat(
            context.getString(R.string.message_time_format)
        )
        simpleDateFormat.format(time)
    }
}

fun isNotifyDeliveredMessage(username:String, message: Message) : Boolean {
    val sender:String = message.from.resourceOrEmpty!!.toString()
    return sender != username && sender != "Mr. Bot"
}

fun isNotifyReadMessageStatus(username:String, message: Message) : Boolean {
    val sender:String = message.from.resourceOrEmpty!!.toString()
    val extension: ExtensionElement? = message.getExtension(DeliveryReceipt.NAMESPACE)
    return extension is DeliveryReceiptRequest && sender != username
}

fun isButtonCommandMessage(sender:String, message: Message) : Boolean {
    message.getExtension("urn:xmpp:json:0") as StandardExtensionElement? ?: return false
    return message.getExtension("urn:xmpp:json:0") is StandardExtensionElement && sender != message.from.resourceOrEmpty.toString()
}

fun getButtonCommands(message: Message) : ArrayList<Button>? {
    val buttonsExtension: StandardExtensionElement =
        message.getExtension("urn:xmpp:json:0") as StandardExtensionElement? ?: return ArrayList()

    val jsonString:String = buttonsExtension.text ?: return ArrayList()
    return try {
        Klaxon().parseArray<Button>(jsonString) as ArrayList
    }
    catch (e: Exception) {
        null
    }
}