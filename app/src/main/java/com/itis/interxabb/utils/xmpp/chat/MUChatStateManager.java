package com.itis.interxabb.utils.xmpp.chat;

import org.jivesoftware.smack.AsyncButOrdered;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.*;
import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.EntityFullJid;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MUChatStateManager extends Manager {
    private static final Logger LOGGER = Logger.getLogger(ChatStateManager.class.getName());

    public static final String NAMESPACE = "http://jabber.org/protocol/chatstates";

    private static final Map<XMPPConnection, MUChatStateManager> INSTANCES = new WeakHashMap<>();

    private static final StanzaFilter INCOMING_MESSAGE_FILTER =
            new AndFilter(MessageTypeFilter.GROUPCHAT, FromTypeFilter.ENTITY_FULL_JID);
    private static final StanzaFilter INCOMING_CHAT_STATE_FILTER = new AndFilter(INCOMING_MESSAGE_FILTER, new StanzaExtensionFilter(NAMESPACE));

    private final Set<MUChatStateListener> chatStateListeners = new HashSet<>();


    private final AsyncButOrdered<MultiUserChat> asyncButOrdered = new AsyncButOrdered<>();


    public static synchronized MUChatStateManager getInstance(final XMPPConnection connection) {
        MUChatStateManager manager = INSTANCES.get(connection);
        if (manager == null) {
            manager = new MUChatStateManager(connection);
            INSTANCES.put(connection, manager);
        }
        return manager;
    }


    private MUChatStateManager(XMPPConnection connection) {
        super(connection);

        connection.addSyncStanzaListener(stanza -> {
            final Message message = (Message) stanza;

            EntityFullJid fullFrom = message.getFrom().asEntityFullJidIfPossible();
            EntityBareJid bareFrom = fullFrom.asEntityBareJid();

            final MultiUserChat chat = MultiUserChatManager.getInstanceFor(connection()).getMultiUserChat(bareFrom);
            ExtensionElement extension = message.getExtension(NAMESPACE);
            String chatStateElementName = extension.getElementName();

            ChatState state;
            try {
                state = ChatState.valueOf(chatStateElementName);
            } catch (Exception ex) {
                LOGGER.log(Level.WARNING, "Invalid chat state element name: " + chatStateElementName, ex);
                return;
            }
            final ChatState finalState = state;

            List<MUChatStateListener> listeners;
            synchronized (chatStateListeners) {
                listeners = new ArrayList<>(chatStateListeners.size());
                listeners.addAll(chatStateListeners);
            }

            final List<MUChatStateListener> finalListeners = listeners;
            asyncButOrdered.performAsyncButOrdered(chat, () -> {
                for (MUChatStateListener listener : finalListeners) {
                    listener.stateChanged(chat, finalState, message);
                }
            });
        }, INCOMING_CHAT_STATE_FILTER);

        ServiceDiscoveryManager.getInstanceFor(connection).addFeature(NAMESPACE);
    }

    /**
     * Register a ChatStateListener. That listener will be informed about changed chat states.
     *
     * @param listener chatStateListener
     * @return true, if the listener was not registered before
     */
    public boolean addChatStateListener(MUChatStateListener listener) {
        synchronized (chatStateListeners) {
            return chatStateListeners.add(listener);
        }
    }

    /**
     * Unregister a ChatStateListener.
     *
     * @param listener chatStateListener
     * @return true, if the listener was registered before
     */
    public boolean removeChatStateListener(MUChatStateListener listener) {
        synchronized (chatStateListeners) {
            return chatStateListeners.remove(listener);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MUChatStateManager that = (MUChatStateManager) o;

        return connection().equals(that.connection());

    }

    @Override
    public int hashCode() {
        return connection().hashCode();
    }


    public interface MUChatStateListener {
        void stateChanged(MultiUserChat chat, ChatState finalState, Message message);
    }
}