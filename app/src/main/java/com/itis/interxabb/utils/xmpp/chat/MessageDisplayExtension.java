package com.itis.interxabb.utils.xmpp.chat;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.List;
import java.util.Map;

public class MessageDisplayExtension implements ExtensionElement {

    public static final String NAMESPACE = "message:display";
    public static final String ELEMENT = "display";

    private String STATE = null;
    private static final String STATE_MESSAGE = "display";

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public CharSequence toXML(String enclosingNamespace) {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.attribute(STATE_MESSAGE, STATE_MESSAGE);
        xml.closeEmptyElement();
        return xml;
    }

    public void setStateMessage(String state) {
        STATE = state;
    }

    public static class MessageDisplayrovider extends EmbeddedExtensionProvider<MessageDisplayExtension> {
        @Override
        protected MessageDisplayExtension createReturnExtension(String currentElement,
                                                                String currentNamespace,
                                                                Map<String, String> attributeMap, List<? extends ExtensionElement> content) {
            MessageDisplayExtension messageDisplayExtension = new MessageDisplayExtension();
            messageDisplayExtension.setStateMessage(attributeMap.get(STATE_MESSAGE));
            return messageDisplayExtension;
        }
    }
}
