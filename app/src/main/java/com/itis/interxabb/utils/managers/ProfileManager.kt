package com.itis.interxabb.utils.managers

import android.content.Context
import android.content.SharedPreferences

data class Profile(val username:String,
                   val password:String)

class ProfileManager(context: Context) {

    companion object {
        private const val preferencesName = "profile_preferences"
        private const val usernameKey     = "username"
        private const val passwordKey     = "password"

        private const val defaultUsername = "user1"
        private const val defaultPassword = "123"
    }

    private val profilePreferences: SharedPreferences = context
        .getSharedPreferences(preferencesName, Context.MODE_PRIVATE)

    fun readProfile() : Profile {
        val username:String = profilePreferences.getString(
            usernameKey,
            defaultUsername
        ) ?: ""
        val password:String = profilePreferences.getString(
            usernameKey,
            defaultPassword
        ) ?: ""

        return Profile(username, password)
    }

    fun saveProfile(username: String, password: String) {
        profilePreferences.edit()
            .putString(usernameKey, username)
            .putString(passwordKey, password)
            .apply()
    }

    fun getUsername() : String {
        return profilePreferences.getString(
            usernameKey,
            defaultUsername
        ) ?: ""
    }

    fun getPassord() : String {
        return profilePreferences.getString(
            passwordKey,
            defaultPassword
        ) ?: ""
    }
}