package com.itis.interxabb.utils.api

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import java.io.Serializable

data class AuthForm(val user:String) : Serializable

class ApiAuthService(private val apiConnection: ApiConnection?) {
    private interface AuthService {
        @POST("/api/chat")
        fun auth(@Body authForm:AuthForm) : Call<UserInfo>
    }

    interface AuthCallback {
        fun onAuth(userInfo: UserInfo)
        fun onError()
    }

    private var call:Call<UserInfo>? = null

    fun auth(username:String, authCallback: AuthCallback) {
        call = apiConnection!!.retrofit!!.create(AuthService::class.java)
            .auth(AuthForm(username))

        call!!.enqueue(object : Callback<UserInfo> {
            override fun onFailure(call: Call<UserInfo>, t: Throwable) {
                authCallback.onError()
            }

            override fun onResponse(call: Call<UserInfo>, response: Response<UserInfo>) {
                if(response.body() != null) {
                    authCallback.onAuth(response.body() as UserInfo)
                }
            }
        })
    }

    fun cancel() {
        if(!call!!.isCanceled) {
            call!!.cancel()
        }
    }
}