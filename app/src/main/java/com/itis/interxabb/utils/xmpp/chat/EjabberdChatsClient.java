package com.itis.interxabb.utils.xmpp.chat;

import com.google.gson.Gson;
import com.itis.interxabb.models.Button;
import com.itis.interxabb.utils.xmpp.EjabberdClient;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.StandardExtensionElement;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension;
import org.jivesoftware.smackx.muc.MucEnterConfiguration;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatException;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.xevent.packet.MessageEvent;
import org.jivesoftware.smackx.xevent.provider.MessageEventProvider;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Resourcepart;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.HashMap;
import java.util.Set;

public class EjabberdChatsClient {

    private class GroupsChatContainer {
        MultiUserChat multiUserChat;
        MessageListener messageListener;

        GroupsChatContainer(MultiUserChat multiUserChat, MessageListener messageListener) {
            this.multiUserChat = multiUserChat;
            this.messageListener = messageListener;
        }
    }

    private XMPPConnection connection;

    private HashMap<String, GroupsChatContainer> groupsChat;

    public EjabberdChatsClient(EjabberdClient client) {
        this.connection     = client.getConnection();
        groupsChat          = new HashMap<>();

        ProviderManager.addExtensionProvider(
                MessageEvent.ELEMENT,
                MessageEvent.NAMESPACE,
                new MessageEventProvider()
        );

        ProviderManager.addExtensionProvider(
                DeliveryReceiptRequest.ELEMENT,
                new DeliveryReceiptRequest().getNamespace(),
                new DeliveryReceiptRequest.Provider());
    }

    public MultiUserChat openGroupChat(String chatJid,
                                       String username,
                                       MessageListener groupChatListener) {
        try {
            EntityBareJid jid = JidCreate.entityBareFrom(chatJid);
            MultiUserChatManager multiUserChatManager = MultiUserChatManager.getInstanceFor(connection);
            MultiUserChat multiUserChat = multiUserChatManager.getMultiUserChat(jid);

            MucEnterConfiguration.Builder mucConfigBuilder = multiUserChat
                    .getEnterConfigurationBuilder(Resourcepart.from(username));
            mucConfigBuilder.requestMaxStanzasHistory(Integer.MAX_VALUE);

            multiUserChat.createOrJoin(mucConfigBuilder.build());
            multiUserChat.addMessageListener(groupChatListener);

            GroupsChatContainer container = new GroupsChatContainer(multiUserChat, groupChatListener);
            groupsChat.put(chatJid, container);
            return multiUserChat;
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        } catch (MultiUserChatException.MucAlreadyJoinedException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (MultiUserChatException.NotAMucServiceException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String sendMessage(String chatId, String messageText, ReceiptReceivedListener receivedListener) throws Exception {
       GroupsChatContainer container = groupsChat.get(chatId);
       if(container == null) throw new Exception("chat is not open");

       MultiUserChat groupChat = container.multiUserChat;

        try {
            Message message = new Message();
            message.setBody(messageText);
            String messageId = DeliveryReceiptRequest.addTo(message);
            message.setStanzaId(messageId);

            DeliveryReceiptManager receiptManager = DeliveryReceiptManager.getInstanceFor(connection);
            receiptManager.setAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
            receiptManager.autoAddDeliveryReceiptRequests();
            receiptManager.addReceiptReceivedListener(receivedListener);

            groupChat.sendMessage(message);
            return messageId;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void notifyStatusChat(String chatId, ChatState chatState) throws Exception {
        GroupsChatContainer container = groupsChat.get(chatId);
        if(container == null) throw new Exception("chat is not open");

        MultiUserChat groupChat = container.multiUserChat;

        Message statusPacket = new Message();
        statusPacket.setBody("");
        statusPacket.setType(Message.Type.groupchat);
        statusPacket.setTo(JidCreate.bareFrom(groupChat.getRoom()));
        ChatStateExtension extension = new ChatStateExtension(chatState);
        statusPacket.addExtension(extension);

        connection.sendStanza(statusPacket);
    }

    public void notifyStatusDeliveredMessage(String chatId, String messageId) throws Exception {
        GroupsChatContainer container = groupsChat.get(chatId);
        if(container == null) throw new Exception("chat is not open");

        MultiUserChat groupChat = container.multiUserChat;

        Message statusPacket = new Message();
        statusPacket.setType(Message.Type.groupchat);
        statusPacket.setTo(JidCreate.bareFrom(groupChat.getRoom()));

        MessageEvent messageEvent = new MessageEvent();
        messageEvent.setDisplayed(true);
        statusPacket.addExtension(messageEvent);

        DeliveryReceiptRequest.addTo(statusPacket);
        statusPacket.setStanzaId(messageId);

        connection.sendStanza(statusPacket);
    }

    private class Command {

        public int action;
        public Command(int action) {
            this.action = action;
        }
    }

    public void sendCommand(String chatId, Button button) throws Exception {
        GroupsChatContainer container = groupsChat.get(chatId);
        if(container == null) throw new Exception("chat is not open");

        MultiUserChat groupChat = container.multiUserChat;

        Message command = new Message();
        command.setBody(button.getTitle());
        command.setType(Message.Type.groupchat);
        command.setTo(JidCreate.bareFrom(groupChat.getRoom()));

        String commandString = new Gson().toJson(
                new Command(button.getAction())
        );

        StandardExtensionElement extensionElement = StandardExtensionElement.builder("bot","urn:xmpp:json:0")
                .setText(commandString)
                .build();

        command.addExtension(extensionElement);
        groupChat.sendMessage(command);
    }

    public void disconnectChats() {
        Set<String> keys = groupsChat.keySet();
        for (String key :
                keys) {
            GroupsChatContainer container = groupsChat.get(key);
            if (container != null) {
                container.multiUserChat.removeMessageListener(container.messageListener);
            }
        }
    }

    public boolean isConnection() {
        return connection.isConnected();
    }
}
