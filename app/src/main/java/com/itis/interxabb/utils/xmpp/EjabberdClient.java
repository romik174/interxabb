package com.itis.interxabb.utils.xmpp;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import de.duenndns.ssl.MemorizingTrustManager;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class EjabberdClient {

    public static final String TLS_TYPE_CONNECTION = "TLS";

    private AbstractXMPPConnection connection;

    private String host, domain, resource, typeConnection;
    private int port;
    private Activity activity;

    private ConnectionTask connectionTask;

    public EjabberdClient(Activity activity,
                          String host,
                          int port,
                          String domain,
                          String resource,
                          String typeConnection) {

        this.activity = activity;
        this.host = host;
        this.port = port;
        this.domain = domain;
        this.resource = resource;
        this.typeConnection = typeConnection;
    }

    public void connect(String login, String password, ConnectionListener listener) {
        connectionTask = new ConnectionTask(activity, host, port, domain, resource, login, password, typeConnection, listener);
        connectionTask.execute();
    }

    public void disconnect() {
        if(connectionTask != null && !connectionTask.isCancelled())
            connectionTask.cancel(true);
        if(isConnection()) {
            connection.disconnect();
        }
    }


    public AbstractXMPPConnection getConnection() {
        return connection;
    }


    public boolean isConnection() {
        return connection != null && connection.isConnected();
    }


    class ConnectionTask extends AsyncTask<Void, Void, Void> {
        private Activity activity;
        private int port;
        private String host, domain, resource, login, password, typeConnection;
        private ConnectionListener listener;

        ConnectionTask(Activity activity,
                       String host,
                       int port,
                       String domain,
                       String resource,
                       String login,
                       String password,
                       String typeConnection,
                       ConnectionListener listener) {

            this.activity       = activity;
            this.host           = host;
            this.port           = port;
            this.domain         = domain;
            this.resource       = resource;
            this.login          = login;
            this.password       = password;
            this.listener       = listener;
            this.typeConnection = typeConnection;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                XMPPTCPConnectionConfiguration connectConfig = createConfig(login, password);
                if (connectConfig != null) {
                    connection = new XMPPTCPConnection(connectConfig);
                    connection.addConnectionListener(listener);
                    connection.connect().login();
                } else return null;

            } catch (SmackException e) {
                e.printStackTrace();
                Log.e(getClass().getName(), e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(getClass().getName(), e.getMessage());
            } catch (XMPPException e) {
                e.printStackTrace();
                Log.e(getClass().getName(), e.getMessage());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        private XMPPTCPConnectionConfiguration createConfig(String login, String password) {
            try {
                DomainBareJid jid = JidCreate.domainBareFrom(domain);
                InetAddress address = InetAddress.getByName(host);

                MemorizingTrustManager mtm = new MemorizingTrustManager(activity, null);
                mtm.bindDisplayActivity(activity);

                SSLContext sslContext = SSLContext.getInstance(typeConnection);
                sslContext.init(null,
                        new MemorizingTrustManager[] { mtm },
                        new SecureRandom());

                XMPPTCPConnectionConfiguration.Builder connectionConfigBuilder = XMPPTCPConnectionConfiguration.builder()
                        .setHostAddress(address)
                        .setXmppDomain(jid)
                        .setPort(port)
                        .setCustomSSLContext(sslContext)
                        .setResource(resource)
                        .setHostnameVerifier(
                                mtm.wrapHostnameVerifier(new org.apache.http.conn.ssl.StrictHostnameVerifier()))
                        .setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible)
                        .setUsernameAndPassword(login, password);

                return connectionConfigBuilder.build();
            } catch (XmppStringprepException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public void reconnect() {
        if(connection != null && !connection.isConnected()) {
            try {
                connection.connect().login();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (SmackException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
