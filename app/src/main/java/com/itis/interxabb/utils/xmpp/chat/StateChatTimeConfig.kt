package com.itis.interxabb.utils.xmpp.chat

class StateChatTimeConfig {
    companion object {
        const val handlerMessage: Int = 1
        const val delay: Long = 10000
    }
}