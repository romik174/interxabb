package com.itis.interxabb.utils.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiConnection private constructor(baseUrl:String){
    val retrofit: Retrofit? = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    companion object Factory {
        private var apiConnection:ApiConnection? = null

        fun create(baseUrl: String) : ApiConnection? {
            if(apiConnection == null) {
                apiConnection = ApiConnection(baseUrl)
            }
            return apiConnection
        }
    }
}
