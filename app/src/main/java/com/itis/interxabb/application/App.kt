package com.itis.interxabb.application

import android.app.Application
import com.itis.interxabb.utils.managers.ConnectionManager
import com.itis.interxabb.utils.managers.ProfileManager

class App : Application() {
    private lateinit var profileManager: ProfileManager
    private lateinit var connectionManager: ConnectionManager

    override fun onCreate() {
        super.onCreate()
        profileManager  = ProfileManager(context = applicationContext)
        connectionManager = ConnectionManager(context = applicationContext)
    }

    fun getProfileManager() : ProfileManager {
        return profileManager
    }

    fun getConnectionManager() : ConnectionManager {
        return connectionManager
    }
}