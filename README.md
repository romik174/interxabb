# InterXabb

Создание нового движка чата для компании Интерсвязь

# отправка сообщения о typing...
```
public void notifyStatusChat(String chatId, ChatState chatState) throws Exception {
         GroupsChatContainer container = groupsChat.get(chatId);
         if(container == null) throw new Exception("chat is not open");

         MultiUserChat groupChat = container.multiUserChat;

         Message statusPacket = new Message();
         statusPacket.setBody("");
         statusPacket.setType(Message.Type.groupchat);
         statusPacket.setTo(JidCreate.bareFrom(groupChat.getRoom()));
         ChatStateExtension extension = new ChatStateExtension(chatState);
         statusPacket.addExtension(extension);

         connection.sendStanza(statusPacket);
}
```

# перехват события typing...
```
private fun enableStateChatListener(connection:XMPPConnection) {
         val mucChatStateManager: MUChatStateManager = MUChatStateManager.getInstance(connection)
         mucChatStateManager.addChatStateListener { _, finalState, message ->
             if(message.body.isEmpty()) {
                 this@ChatFragmentPresenter.fragment.activity!!.runOnUiThread {
                     val username = getUsername(message.from)
                     if (finalState == ChatState.composing) {
                         fragment.showTyping(username, message.stanzaId)

                         if(handlerStateTyping.hasMessages(StateChatTimeConfig.handlerMessage)) {
                             handlerStateTyping.removeMessages(StateChatTimeConfig.handlerMessage)
                         }

                         handlerStateTyping.sendEmptyMessageDelayed(
                             StateChatTimeConfig.handlerMessage,
                             StateChatTimeConfig.delay
                         )
                     }
                     else if (finalState == ChatState.paused) {
                         fragment.removeTyping()
                         if(handlerStateTyping.hasMessages(StateChatTimeConfig.handlerMessage)) {
                             handlerStateTyping.removeMessages(StateChatTimeConfig.handlerMessage)
                         }
                     }
                 }
             }
         }
}
```
класс `MUChatStateManager` находится в `./utils/xmpp/chat`


# перехват события о том, что сообщение прочтено

```
val extension:ExtensionElement? = message.getExtension(DeliveryReceipt.NAMESPACE)
    if(extension is DeliveryReceiptRequest) {
        fragment.setReadMessageStatus(
        messageId = message.stanzaId,
        status = true
    )
}
```

